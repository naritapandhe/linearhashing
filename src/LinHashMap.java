package com.company;
/************************************************************************************
 * @file LinHashMap.java
 *
 * @author  John Miller
 */

import java.io.*;
import java.lang.reflect.Array;

import static java.lang.System.out;

import java.util.*;



/************************************************************************************
 * This class provides hash maps that use the Linear Hashing algorithm. A hash
 * table is created that is an array of buckets.
 */
public class LinHashMap<K, V> extends AbstractMap<K, V> implements
		Serializable, Cloneable, Map<K, V> {
	/**
	 * The number of slots (for key-value pairs) per bucket.
	 */
	private static final int SLOTS = 2;

	/**
	 * The class for type K.
	 */
	private final Class<K> classK;

	/**
	 * The class for type V.
	 */
	private final Class<V> classV;

	/********************************************************************************
	 * This inner class defines buckets that are stored in the hash table.
	 */
	private class Bucket implements
	Serializable {
		
		//Represents no. of keys
		int nKeys;
		//Represents the original keys, values
		K[] key;V[] value;
		
		
		Bucket next;
		int bucketNo;
		int myMod;

		@SuppressWarnings("unchecked")
		Bucket(Bucket n) {
			nKeys = 0;
			key = (K[]) Array.newInstance(classK, SLOTS);
			value = (V[]) Array.newInstance(classV, SLOTS);
			next = n;
			bucketNo = -1;
			myMod=-1;
		} // constructor

	} // Bucket inner class
	
	
	private  int staticTest=0;

	/**
	 * The list of buckets making up the hash table.
	 */
	private final List<Bucket> hTable;

	/**
	 * The modulus for low resolution hashing
	 */
	private int mod1;

	/**
	 * The modulus for high resolution hashing
	 */
	private int mod2;

	/**
	 * Counter for the number buckets accessed (for performance testing).
	 */
	private int count = 0;

	/**
	 * The index of the next bucket to split.
	 */
	private int split = 0;
	
	
	/********************************************************************************
	 * Construct a hash table that uses Linear Hashing.
	 * 
	 * @param _classK the class for keys (K)
	 * @param _classV the class for keys (V)
	 * @param initSize the initial number of home buckets (a power of 2, e.g., 4)
	 */
	public LinHashMap(Class<K> _classK, Class<V> _classV, int initSize) {
		classK = _classK;
		classV = _classV;
		hTable = new ArrayList<>();
		mod1 = initSize;
		mod2 = 2 * mod1;

		for (int i = 0; i < initSize; i++) {
			Bucket b = new Bucket(null);
			b.bucketNo = i;
			hTable.add(i, b);
			b.myMod=mod1;
		}
		out.println(staticTest++);
		

		
	} // constructor

	/********************************************************************************
	 * Return a set containing all the entries as pairs of keys and values.
	 * 
	 * @return the set view of the map
	 */
	
	
	public Set<Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<K, V>> enSet = new HashSet<>();

		for (int i = 0; i < hTable.size(); i++) {
			int j=0;
            for (Bucket b = hTable.get(i); b != null; b = b.next) {
            	//Check if this can be replaced
            	while(j<b.nKeys){
            		enSet.add (new AbstractMap.SimpleEntry <K, V> (b.key[j], b.value[j]));
            		j++;
            	}
            } // for
        } // for
            
		return enSet;
	} // entrySet

	
	/********************************************************************* GET *************************************************************/
	/*******************************************************************************
	 * Given the key, look up the value in the hash table.
	 * 
	 * @param key the key used for look up
	 * @return the value associated with the key
	 */
	public V get(Object key) {
		int i = h(key);
		
		if(i<split){
			i=h2(key);
		}
			
		return getKey(key, i);
	}
	
	/*******************************************************************************
	 * Given the key and index, check if the key exists at that particular.
	 * If not, look for the key in chained buckets.
	 * 
	 * @param key the key used for look up
	 * @param i   the index position of the key
	 * @return the value associated with the key
	 */
	private V getKey(Object key, int i) {
		Bucket b = hTable.get(i);
		// If the Bucket we retrieve is NULL, implies we searched entire table
		// and key not found
		// Hence return NULL (Also its the breaking condition for the recursion)
		if (b == null){
			//System.out.println("Element not found!!");
			return null;
		}

		//Check if found in the current bucket
		while (b != null) {

			count++;
			// Get the keys
			Comparable[] keyArray = (Comparable[]) b.key;
			// Check if the required key exists in the index
			for (int k = 0; k < b.nKeys; k++) {
				if (compareKey(keyArray[k], key)) {
					// System.out.print("Bucket{"+i+"}=> ");
					return getValueFromKey(k, b.value);
				}
			}

			// To check for key in the linked overflow buckets
			b = b.next;

		}
					
		System.out.println("Element not found!!");
		return null;
	}

	

	/*******************************************************************************
	 * Given the required key, look up the value in the hash table.
	 * 
	 * @param originalKey the keys used in hash table
	 * @param keyToSearch the keys that is to be looked in the hash table
	 * @return true or false
	 */
	private boolean compareKey(Comparable originalKey, Object keyToSearch) {
		if (originalKey.compareTo(keyToSearch) == 0) {
			return true;
		}
		return false;
	}

	/*******************************************************************************
	 * Given the index(i.e. slot position), get the respective value from the bucket
	 * 
	 * @param index the slot in the bucket
	 * @param valueArray values of the bucket
	 * @return Value
	 */
	private V getValueFromKey(int index, V[] valueArray) {
		V retVal=null;
		
		//Check if the value exists, if yes, return it
		if(valueArray[index]!=null){
		 		retVal=valueArray[index];
		 		
		}
		
		return retVal;
	}
	/*************************************************************** GET End *************************************************************/
	

	
	/*************************************************************** PUT Start *************************************************************/
	
	/********************************************************************************
	 * Put the key-value pair in the hash table.
	 * 
	 * @param key the key to insert
	 * @param value the value to insert
	 * @return null (not the previous value)
	 */
	public V put(K key, V value) {
		int bucketIndex = h(key);
		if(bucketIndex<split){
			bucketIndex=h2(key);
		}

//		System.out.println(bucketIndex);
//		System.out.println("mod1: " + mod1 + ", mod2: " + mod2);

		Bucket b = hTable.get(bucketIndex);
		putIntoBucket(b, key, value);
		/**
		 * Check if after adding the element, threshold is exceeded.
		 * If yes, split the bucket and rehash its elements.
		 */
//		if (isThresholdExceeded()) {
//
//			splitBucketAndUpdateMod();
//			rehash();
//
//		}// End of policy

		return null;
	} // put

	
	/*******************************************************************************
	 * Given the index, put the key-value inside a SLOT inside a bucket
	 * 
	 * @param b (index) the slot in the bucket
	 * @param key   the key to be inserted in the bucket
	 * @param value the value to be inserted in the bucket
	 * @return void
	 */
	private void putIntoBucket(Bucket b, K key, V value) {
		if (b.nKeys < SLOTS) {
			b.key[b.nKeys] = key;
			b.value[b.nKeys++] = value;
		} else {
			Bucket temp=b;
			do{
				if (temp.next == null) {
					Bucket newBucket = new Bucket(null);
					temp.next = newBucket;
					newBucket.myMod=temp.myMod;
					putIntoBucket(newBucket, key, value);
					break;
				} else {
					if(temp.next.nKeys<SLOTS){
						temp.next.key[temp.next.nKeys] = key;
						temp.next.value[temp.next.nKeys++] = value;
						break;
					}else{
						temp=temp.next;
					}
					
				}
				
			}while(true);
			
			//After adding the key-value, check if the bucket split is required
			// and rehash the existing hash map
			splitBucketAndUpdateMod();
			
		}
	}

	/*******************************************************************************
	 * Split the bucket pointed by split and update the respective mod functions 
	 * 
	 * @return void
	 */
	private void splitBucketAndUpdateMod() {
		int tempSplit=split;
		
		//1. Get the bucket which is to be split
		Bucket bucketToBeSplit = hTable.get(tempSplit);

		//2. Add a new bucket
		Bucket newBucket = new Bucket(null);
		hTable.add(newBucket);
		
		//3. Link the buckets
		newBucket.bucketNo = hTable.size() - 1;
		
		//4. Change the mod factor
		bucketToBeSplit.myMod = 2 * bucketToBeSplit.myMod;
		newBucket.myMod = bucketToBeSplit.myMod;
		
		split++;
		if(checkAllMod()){
			split=0;
			mod1=2*mod1;
			mod2 = 2*mod2;
		}
		//5. Rehash the elements and increment the split
		rehash(bucketToBeSplit,tempSplit);

		
	}
	
	/*******************************************************************************
	 * All elements in the bucket are re-mapped to their corresponding buckets
	 * after a change in the mod function after a bucket is split. 
	 * 
	 * @return void
	 */
	private void rehash(Bucket bucketToBeSplit,int currentSplitIndex) {
		Bucket b = bucketToBeSplit;
		int elemCounter=0;
		boolean flagTotalCount=false;
		int mod=b.myMod;
		
		do{
			
			//Rehash current bucket
			for (int i = 0; i < b.nKeys; ) {
				elemCounter++;
				K currentKey = b.key[i];
				V currentValue = b.value[i];

				int index = rH(currentKey, mod);

				/**
				 * If the element after rehashing according to the new mod function
				 * falls into a different bucket.
				 */
				if (currentSplitIndex != index) {
					
					Bucket rehashedBucket = hTable.get(index);
					putIntoBucket(rehashedBucket, currentKey, currentValue);

					// Calculating the number of elements to be moved i.e.
					arrayCopy(b, i);
					
				} else {
					i++;
					
				}			

			}
			b=b.next;
		}while(b!=null);
		
		
		
//		b=b.next;
//		while(b!=null){
//			rehash(b,currentSplitIndex);
//			b=b.next;
//		}	
		
			//Rehash current bucket
//			for (int i = 0; i < b.nKeys; ) {
//				elemCounter++;
//				K currentKey = b.key[i];
//				V currentValue = b.value[i];
//
//				int index = rH(currentKey, b.myMod);
//
//				/**
//				 * If the element after rehashing according to the new mod function
//				 * falls into a different bucket.
//				 */
//				if (tempSplit != index) {
//					
//					Bucket rehashedBucket = hTable.get(index);
//					putIntoBucket(rehashedBucket, currentKey, currentValue);
//
//					// Calculating the number of elements to be moved i.e.
//					arrayCopy(b, i);
//					
//				} else {
//					i++;
//					
//				}
//			}
//			
//		}
		

	}
	
	/*******************************************************************************
	 * Moves the elements within the arrays.
	 * If any of the arrays are empty, then values from overflow arrays are moved
	 * 
	 * @return void
	 */
	  private void arrayCopy(Bucket b, int j) {

			b.nKeys--;
			int elementsToMove = b.key.length - (j + 1);
			if(elementsToMove > 0){
				System.arraycopy(b.key, j + 1, b.key, j, elementsToMove);
				System.arraycopy(b.value, j + 1, b.value, j, elementsToMove);
			}
			b.key[b.nKeys] = null;
			b.value[b.nKeys] = null;

			do{
				
				if (b.nKeys < SLOTS && b.next!=null && b.next.nKeys>0) {
					Bucket next=b.next;
					if(next.nKeys > 0){

						int elementsToTransfer = (SLOTS - b.nKeys);	
						if(elementsToTransfer>0){
							System.arraycopy(next.key, 0, b.key, b.nKeys, elementsToTransfer);
							System.arraycopy(next.value, 0, b.value, b.nKeys,elementsToTransfer);
							b.nKeys += elementsToTransfer;
		
							System.arraycopy(next.key, elementsToTransfer,next.key, 0, (next.nKeys - elementsToTransfer));
							System.arraycopy(next.value, elementsToTransfer,next.value, 0, (next.nKeys - elementsToTransfer));
							
							Arrays.fill(next.key, (next.nKeys - elementsToTransfer), (next.key.length), null);
							Arrays.fill(next.value, (next.nKeys - elementsToTransfer), (next.value.length), null);
							next.nKeys -= elementsToTransfer;
						}

						b=next;
					}
				}else{
					break;
				}
			}while(b.next!=null);
	  }
			
	/*******************************************************************************
	 * Checks if all the buckets have the same local depth
	 * @return true or false
	 */
	private boolean checkAllMod(){
		for(int i = 0; i < mod1; i++)
	    {
	        if(hTable.get(i).myMod != hTable.get(0).myMod)
	            return false;
	    }
		if(mod1!=hTable.get(0).myMod)
			return true;
		
		return false;
		
	}
	
	/*******************************************************************************
	 * Checks if the ratio of the number of elements to twice the no. of buckets
	 * exceeds a threshold value (here 0.70). If yes, elements need to be rehashed.
	 * 
	 * @return void
	 */
	private boolean isThresholdExceeded() {
		int numberOfElements = totalNumberOfElements();
		double policyCheck = ((double) numberOfElements / (double) ((hTable.size() * 2)));
		
		if (policyCheck > 0.7)
			return true;

		return false;

	}

	/*******************************************************************************
	 * Function to get the total no. of elements in all the buckets
	 * 
	 * @return int
	 */
	public int totalNumberOfElements(){
		int numberOfElements=0;
		//Find the total number of elements
		for (int k = 0; k < hTable.size(); k++) {
			Bucket b = hTable.get(k);
			while (b != null) {
				numberOfElements += b.nKeys;
				b = b.next;
			}
		}

		return numberOfElements;

	}
	
	/********************************************************************************
	 * Return the size (SLOTS * number of home buckets) of the hash table.
	 * 
	 * @return the size of the hash table
	 */
	public int size() {
		int count=0;
		for(int i=0;i<hTable.size();i++){
			if(hTable.get(i).nKeys>0){
				count+=hTable.get(i).nKeys;
			}
		}
		return count;
		//return SLOTS * (mod1 + split);
		
	} // size

	/********************************************************************************
	 * Prints the hash table.
	 */
	public void print() {
		out.println("Hash Table (Linear Hashing)");
		out.println("-------------------------------------------");

		ListIterator<Bucket> litr = hTable.listIterator();
		while (litr.hasNext()) {
			Bucket b = litr.next();
			System.out.print("Bucket {" + b.bucketNo + "}=> ");
			while (b != null) {
				for (int i = 0; i < b.nKeys; i++) {
					if (i < (b.nKeys - 1))
						System.out.print(b.value[i] + "->");
					else if(b.next!=null)
						System.out.print(b.value[i]+"->");
					else
						System.out.print(b.value[i]);
				}
				b = b.next;
			}
			System.out.println();
			//System.out.print("\t||");
			System.out.println();
		}

		out.println("-------------------------------------------");
	} // print

	/********************************************************************************
	 * Hash the key using the low resolution hash function.
	 * 
	 * @param key the key to hash
	 * @return the location of the bucket chain containing the key-value pair
	 */
	private int h(Object key) {
		int result = key.hashCode();
		return generateIndex(result,mod1);
	} // h

	/********************************************************************************
	 * Hash the key using the high resolution hash function.
	 * 
	 * @param key the key to hash
	 * @return the location of the bucket chain containing the key-value pair
	 */
	private int rH(Object key, int mod) {
		int result = key.hashCode();
		return generateIndex(result,mod);
	} // h

	/********************************************************************************
	 * Hash the key using the high resolution hash function.
	 * 
	 * @param key the key to hash
	 * @return the location of the bucket chain containing the key-value pair
	 */
	private int h2(Object key) {
		int result = key.hashCode();
		return generateIndex(result,mod1*2);
		
	} // h2

	/********************************************************************************
	 * Handles the negative values for hashcode
	 * 
	 * @param result the hashed value
	 * @param mod mod values
	 * @return the location of the bucket chain containing the key-value pair
	 */
	private int generateIndex(int result,int mod){
		int pos=-1;
		if (result >= 0)
			pos = result % mod;
		else
			pos = -result % mod;

		return pos;
		
	}
	
	/********************************************************************************
	 * The main method used for testing.
	 * @param args	the command-line arguments (args [0] gives number of keys to insert)
	 * 
	 */
	public static void main(String[] args) {
		LinHashMap<Integer, Integer> ht1 = new LinHashMap<>(Integer.class,Integer.class,2);
		LinHashMap<String, String> ht2= new LinHashMap<>(String.class,String.class,2);
		
		//out.println(ht1.staticTest);
		//out.println(ht2.staticTest);
		
		
		return; 
		
//		int nKeys = 30;
//		if (args.length == 1)
//			nKeys = Integer.valueOf(args[0]);
//		int[] input=new int[]{3, 2, 4, 1, 8,14,5,10,7,24,17,13,15};
//		int[] input=new int[]{8,13,10,15,19,22};
//		int[] input=new int[]{18,10,7,14,8,9,21};
//		int[] input=new int[]{2,3,5,4,7,1,6};
		
		
//		String[] input = new String[] { "N","E","W","T","G","I","R","C","H"};
//		int[] input=new int[] {
//				630846,
//				139320,
//				520667,
//				865412,
//				80294,
//				532823,
//				655620,
//				885893,
//				760652,
//				610742,
//				725316,
//				546595,
//				796199,
//				958551,
//				897569,
//				439406,
//				620048,
//				337304,
//				677893,
//				966614,	
//				60000 };
		

//		for (int i = 0; i < input.length; i++) {
//			ht.put(input[i], input[i]);
//		}
//		
//		
//		ht.print();
//		//out.println ("-------------------GET---------------------");
//		out.println (ht.get(139320));
//		out.println ("Number of buckets accessed = " + ht.count);
//		out.println ("Average number of buckets accessed = " + ht.count / (double) input.length);
//		
	} // main

} // LinHashMap class
